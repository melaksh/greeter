package com.example.greeter;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class GreeterApplicationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	// @Test
	// public void greetingShouldReturnDefaultMessage() throws Exception {
	// 	assertThat(this.restTemplate.getForObject("http://localhost:"+this.port+"/greeting", String.class)).contains("Hello World");
	// }

	// @Test
	// public void greetingShouldReturnGivenName() throws Exception {
	// 	String name = "laksh";
	// 	assertThat(this.restTemplate.getForObject("http://localhost:"+this.port+"/greeting?name="+name, String.class)).contains("Hello "+name);
	// }

	@Test
	public void greetingShouldNotBeNull() throws Exception {
		assertThat(new Greeting(1, "laksh")).isNotNull();
	}


}
